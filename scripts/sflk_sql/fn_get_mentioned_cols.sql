/*
* Used for identifying and retuning the columns used in the query as an
* array.
*/
CREATE OR REPLACE FUNCTION FN_GET_MENTIONED_COLS(P_TBL STRING, P_COLS ARRAY, P_ALIAS_COLS ARRAY, P_TKNS ARRAY)
    RETURNS ARRAY
    LANGUAGE JAVASCRIPT
AS
$$
    function fn_get_mentioned_cols(p_TBL, p_COLS_ARR, p_ALIAS_COLS_ARR, p_TKNS) {
      var mcols = [];

      var  tkns_prefixed = p_TKNS.map(w => {
          return p_TBL + '::' + w;
      });
      //console.log(tkns_prefixed);

      mcols = p_COLS_ARR.filter(x => tkns_prefixed.includes(x));

      //filter cols from alias, if its current table
      acols = p_ALIAS_COLS_ARR.filter(x => x.search(p_TBL) != -1);

      mcols = mcols.concat(acols);

      return Array.from(new Set(mcols));
  }

   return fn_get_mentioned_cols(P_TBL, P_COLS, P_ALIAS_COLS, P_TKNS)
$$
;
