/*
* SQL queries are not necessarily standardized, meaning you can find variation
* in adoption patterns like :
*  - Single / multi-line comments
*  - Case handling
*  - Spacing / formatting issues
*  - Multiple statements, buched together and executed in a single execution.
*
* We need to cleanse these to a manageable format. Hence this function aids in
* achieving this.
*/
CREATE OR REPLACE FUNCTION FN_SQL_QRYTXT_CLEANSE(QRY_TEXT_RAW STRING)
  RETURNS STRING
  LANGUAGE JAVASCRIPT
AS
$$
    var qry_1 = QRY_TEXT_RAW.toUpperCase();

    // identify and remove single line comments
    var qry_2 = qry_1.replace(/--.*/g, ' ').replace(/\/\/.*/g, ' ');

    //remove multi line comments
    var qry_3 = qry_2.replace(/\/\*(.|\n)+\*\//, ' ');

    //replace line breaks & quotations
    var qry_4 = qry_3.replace(/\n/g,' ').replace(/\"/g,'');

    //replace multi space
    var qry_5 = qry_4.replace(/\s{2,}/g,' ');

    //in scenarios where there are multi sql statement execution
    //each statement would be seperated by a '; '. Assuming that
    //this seperator is not used other than the above, lets break
    //the statements up on a per statement basis
    var qry_6 = qry_5.replace(/;/g,';\n');

    return qry_6.trim();
$$
;
