/*
* The tokenization function preserves declarations such as <db>.<sch>.<table name>
* or <sch>.<table name> . for ex:
*   UTIL_DB.SFLK_CATALOG.ACCOUNT_USAGE_TABLES
* We identify such tokens and extract the mentioned database from the tokens.
*/
CREATE OR REPLACE FUNCTION FN_GET_MENTIONED_DBS(P_TKNS ARRAY, P_DB STRING)
  RETURNS ARRAY
  LANGUAGE JAVASCRIPT
AS
$$
    function fn_get_mentioned_dbs(p_TKNS, p_DB) {
      var dbs = [];

      //initialize with the current db
      if (p_DB !== undefined)
          dbs.push(p_DB);

      p_TKNS.forEach(w => {
          var dot_cnt = (w.match(/\./g) || []).length;
          if (dot_cnt == 2)
              dbs.push(w.split('.')[0]);
              // console.log(w.split('.')[0]);
      });

      return Array.from(new Set(dbs));
    }

    return fn_get_mentioned_dbs(P_TKNS, P_DB);
$$
;

/*
* The tokenization function preserves declarations such as <db>.<sch>.<table name>
* or <sch>.<table name> . for ex:
*   UTIL_DB.SFLK_CATALOG.ACCOUNT_USAGE_TABLES
* We identify such tokens and extract the mentioned database & schemas from the tokens.
*/
CREATE OR REPLACE FUNCTION FN_GET_MENTIONED_DB_AND_SCHEMAS(P_TKNS ARRAY, P_DB STRING, P_SCH STRING)
  RETURNS ARRAY
  LANGUAGE JAVASCRIPT
AS
$$
    function fn_get_mentioned_db_and_schemas(p_TKNS, p_DB, p_SCH) {
      var dbs = [];

      //initialize with the current db
      if ((p_DB !== undefined) && (p_SCH !== undefined))
          dbs.push(p_DB + '.' + p_SCH);


      p_TKNS.forEach(w => {
          var dot_cnt = (w.match(/\./g) || []).length;
          if (dot_cnt == 2)
              dbs.push(
                  w.split('.')[0] + '.' + w.split('.')[1]
                  );
          if ((dot_cnt == 1) && (p_DB !== undefined))
             dbs.push(p_DB + '.' + w.split('.')[0]);
      });

      return Array.from(new Set(dbs));
  }

  return fn_get_mentioned_db_and_schemas(P_TKNS, P_DB, P_SCH);
$$
;

/*
* To aid in matching to exact tables, we prefix each token with database &
* schema, if its not already present in the token.
*/
CREATE OR REPLACE FUNCTION FN_MENTIONED_TABLES(TKNS ARRAY, DB STRING, SCH STRING)
  RETURNS ARRAY
  LANGUAGE JAVASCRIPT
AS
$$
    var tkns = TKNS;
    var ret_tkns = [];

    tkns.filter(x => x != '*')
    .forEach(item => {
        if ((DB !== undefined) && (SCH !== undefined))
            ret_tkns.push(DB + '.' + SCH + '.' + item);
        if (SCH !== undefined)
            ret_tkns.push(SCH + '.' + item);
    });

    return Array.from(new Set(ret_tkns));
$$
;
