/*
* Tokenizes the query text
*/
CREATE OR REPLACE FUNCTION FN_QRYTXT_TOKENIZE(QRY_TEXT STRING)
  RETURNS ARRAY
  LANGUAGE JAVASCRIPT
AS
$$
    function fn_qrytxt_tokenize(p_QRY_TEXT) {
      //Ref : https://docs.snowflake.com/en/sql-reference/reserved-keywords.html
      var SFLK_KEYWORDS = new Set([
        "ACCOUNT", "ALL", "ALTER", "AND", "ANY", "AS", "BETWEEN", "BY", "CASE", "CAST",
        "CHECK", "COLUMN", "CONNECT", "CONNECTION", "CONSTRAINT", "CREATE", "CROSS",
        "CURRENT", "CURRENT_DATE", "CURRENT_TIME", "CURRENT_TIMESTAMP", "CURRENT_USER",
        "DATABASE", "DELETE", "DISTINCT", "DROP", "ELSE", "EXISTS", "FALSE", "FOLLOWING",
        "FOR", "FROM", "FULL", "GRANT", "GROUP", "GSCLUSTER", "HAVING", "ILIKE", "IN",
        "INCREMENT", "INNER", "INSERT", "INTERSECT", "INTO", "IS", "ISSUE", "JOIN",
        "LATERAL", "LEFT", "LIKE", "LOCALTIME", "LOCALTIMESTAMP", "MINUS", "NATURAL",
        "NOT", "NULL", "OF", "ON", "OR", "ORDER", "ORGANIZATION", "QUALIFY", "REGEXP",
        "REVOKE", "RIGHT", "RLIKE", "ROW", "ROWS", "SAMPLE", "SCHEMA", "SELECT", "SET",
        "SOME", "START", "TABLE", "TABLESAMPLE", "THEN", "TO", "TRIGGER", "TRUE",
        "TRY_CAST", "UNION", "UNIQUE", "UPDATE", "USING", "VALUES", "VIEW", "WHEN",
        "WHENEVER", "WHERE", "WITH",

        //we could put all snowflake functions into the set too, but it is continously
        //growing, hence an upkeep. So deciding to choose only specific of these, as they
        //will be filtered out.

        "SYSTEM", "DATE", "COALESCE", "SUM", "TO_DATE", "ADD", "DAY", "DESC", "LIMIT",
        "IF", "NULL", "MIN", "MAX", "MERGE", "TRUNCATE"
      ]);

      var qry = p_QRY_TEXT.replace(/\n/g,' ');

      var expStr = Array.from(SFLK_KEYWORDS).join("\\b|\\b");

      // remove all keywords
      var q_1 = qry.replace(new RegExp(expStr, 'gi'), '')

      // remove all symbols (need to preserve '.')
      var q_2 = q_1.replace(/[\(,\),\,,+,\-,<,>,=,\',\",:,\*,%]/g, ' ');

       // remove all numbers
      var q_3 = q_2.replace(/ [0-9] /g, ' ');

      // remove multi space and trim out
      var q_4 = q_3.replace(/ +/g, ' ').trim().split(" ");

      //remove duplicates
      var q_tkns = Array.from(new Set(q_4));

      return q_tkns;
    }

    return fn_qrytxt_tokenize(QRY_TEXT);
$$
;
