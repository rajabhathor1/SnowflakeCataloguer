/*
* Ref : https://docs.snowflake.com/en/sql-reference/identifiers-syntax.html
* Based on Snowflake identifier requirements, we could have db object (table,
* columns etc..) with spaces and other ascii characters.
*
* When these characters (for ex: ~, # ...) are part of the column name they
* are typically used with quotes in the sql queries. For ex:
*
* SELECT COLA, "COL B", "COL#C" from "TABLE SPACED NAMES";
*
* To handle this smoothly, we first replace these non-alphanumeric characters
* with its corresponding ascii codes, so that the quotes can be removed. hence
* during our process we would convert the above example to :
*
* SELECT COLA, COL_32_B, COL_35_C from TABLE_32_SPACED_32_NAMES;
*
* This function is used to replace this in queries.
*/
CREATE OR REPLACE FUNCTION FN_RESOLVE_QUOTED_IDENT(QRY_TEXT_RAW STRING)
  RETURNS STRING
  LANGUAGE JAVASCRIPT
AS
$$
    var qry = QRY_TEXT_RAW.toUpperCase();

    function resolve_ident(ident) {
      var t=ident;
      var rx=/\w/;

      if (t === undefined)
        return null;

      var ct = t.split('').map(x => {
          if(rx.exec(x) != null)
              return x;
          return '_'+x.charCodeAt(0)+'_';
      }).join('');

      return ct;
    }

    //tables/Columns can be quoted, as there is a potential for
    //non alphanumeric characters in names. We will be substituting
    //these characters with ascii code.
    var quoted = /[\s|\.]\"(.*?)\"/g,matched, qry_1;
    var qry_1 = qry;
    while(matched = quoted.exec(qry_1)){
        if (matched != null) {
            console.log(matched[1]);
            qry_1 = qry_1.replace(matched[1],
                        resolve_ident(matched[1]));
        }
    }

    //replace quotations
    var qry_2 = qry_1.replace(/\"/g,'');

    return qry_2;
$$
;
