# Installation

## Snowflake Cataloguer
All the scripts related to setup and configuratoins are present in [scripts/sflk_sql/](../scripts/sflk_sql/) folder.

### Pre-Requsite

The script [base.sql](../scripts/sflk_sql/base.sql) helps in configuring minimal needs. 

#### Database and Schema
You would need a location where we need to store and process the data. In the implementation, these are 
  - util_db => database
  - sflk_catalog => schema

#### Role : SFLK_CATALOGUER
Since the queries can contain sensitive information and also that Account usage views should not be allowed by all Snowflake users, we need to create a custom role **SFLK_CATALOGUER**. This role should be assigned to only DBAs.

The role needs to read the views from Snowflake.Account_Usage. By default, the SNOWFLAKE database is available only to the ACCOUNTADMIN role. To enable other roles to access the database and schemas, and query the views, a user with the ACCOUNTADMIN role must grant the following data sharing privilege to the desired roles:
-    IMPORTED PRIVILEGES
- REF : https://docs.snowflake.com/en/sql-reference/account-usage.html#enabling-account-usage-for-other-roles

### Functions
Once the above pre-requisites are configured you would need to deploy all the various custom udf's. These are scripts that are prefixed with fn's like:
- [fn_get_cols_basedon_alias.sql](../scripts/sflk_sql/fn_get_cols_basedon_alias.sql)
- [fn_mentions.sql](../scripts/sflk_sql/fn_mentions.sql)
 etc...

Thats it!! 