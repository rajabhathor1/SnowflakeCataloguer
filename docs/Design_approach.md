## Design Approach
 
### Query Parsing
The SNOWFLAKE.ACCOUNT_USAGE.QUERY_HISTORY view contains the various queries issued by users against any object. Every SQL execution is logged and is available through this view. The actual statement issued is available in the 'QUERY_TEXT' column.

Here is a sample query that was issued:
```sql
SELECT count('x') AS count FROM (
 SELECT foo.SUBSCRIPTION_ID, foo.ITEMS, foo.__SDC_PRIMARY_KEY,
        foo.DATASOURCE //, foo._SDC_SEQUENCE
 FROM ( SELECT *, row_number ()
     OVER (PARTITION BY STAGING_DATA_13_LOADER_SNOW10.SDC_PRIMARY_KEY
     ORDER BY _sdc_sequence DESC) AS rnk
 FROM STAGING_DATA_LOADER ) AS foo
 WHERE rnk = 1)
```
Query parsing helps in understanding lineage, usage, etc. Though SQL is a standard, the exact way it is written and executed has lots of variations. In my development, I have come across queries embedded with comments, multiple line comments, quoted string, etc.

These articles give good insights into the complexities
- [Parsing SQL]()
- [Query parser, an Open Source Tool for Parsing and Analyzing SQL](https://medium.com/r/?url=https%3A%2F%2Feng.uber.com%2Fqueryparser%2F)

#### Snowflake native approach
The above articles are geared towards using libraries and also developer preferred languages of query parsing. Their approach is also geared towards building a centralized data catalog on the enterprise.

While the mentioned approach is perfectly valid, the intent of the current implementation is to keep all processing within Snowflake.

Snowflake does offer using language of choices & libraries via External Functions. Not all clients have necessarily adopted this. It also opens up a need to discuss, collaborate and work with other teams (developers, platform team, enterprise architects) etc., and the need to handle politics, opinion etc..

Hence to keep it simple, the implementation is geared on using what Snowflake had to offer. Hence no extra infrastructure is needed. Adoption of SQL & JS UDF which are Snowflake core capabilities would avoid all those costly political battles too.

We do require a custom role to be created and given access to Snowflake Account Usage views. For this,  you would need some help from the AccountAdmin.

#### Embedded information in queries
Queries issued by actors, might contain PII and other secure information too. When you need to export these outside of Snowflake, because you prefer to do using preferred language choice etc.. You would need to encrypt or remove these secure information from the query text. These could be bringing in additional complexity.

By processing within Snowflake, we can avoid these movements. The output of processing would be information present in the Catalog which are just names of objects (database, schema, tables) and ids, auto generated id of these objects. These in my opinion are less sensitive and hence can be exported or made visible to all Snowflake actors if you choose too.
 
### Frequency of refresh
Unlike data catalog tool like Amundsen or other commercial offerings, this implementation is not geared towards maintaining real-time or near real-time catalog. At best, the catalog is processed with previous day.

This is in part due to how often the Snowflake.Account Usage views get refreshed. And also, reducing operational overheads.

You can typically do the processing once a day or on a demand basis. Based on processing around 1 million records from my lab, an XS warehouse-size was good enough for the complete processing. End to end the process took around 5 min and less.

The materialized tables are all transient, as we can recreate the entire content.

### Who can use the catalog ?
Once the various materialized tables are built, you can expose the tables to be public to all Snowflake users. The choice of exposing is left to you.
My opinion is that if these tables are public; then it would allow the data engineers and operations to answer quickly table dependencies etc.. The time spent on finding these can be reduced a lot and issues get resolved quickly.

### Why not DBT ?
Based on field experience, I had realized that not every company adopts DBT. Reason could range from approval from architects, preffered implementation tool etc.. To allow for a wider approach, I choose to implement this in SQL & UDFs.

In an ideal world, I would prefer to use DBT.

## Enhancements
If there are interest, then the following functionalities can be developed in the future:
    - Identify the subject table onto which UPDATE, INSERT, MERGE are don.
    - Identify tables that get ingested with data. These tables can be referred in COPY INTO, SNOWPIPE usage etc..
    - Tokenize stored proc and function and determine tables/views usage.
    - Data Ingestion timeline
    - Data processing timeline
    
## Current Limitations
#### Query parsing
Current implementation does not solve for every type of scenario or every environment; expect some modifications based on your situation. This mainly is due to limitations on usage patterns, query text. Though such modification would not be a big constraint. 

#### Dataset size and processing time
As mentioned, our Snowflake is a lab account hence the queries is only around 1 million or so. In your org, the count might be more, hence it might take longer than 5 min to process for your env.

#### Temp table
Temporary tables will not be accounted for. As there definition is not available in the Tables/Views catalog.

#### Procedure, Streams, Tasks etc..
SQL statements executed by stored procedures, streams and tasks are currently ignored. Based on how this concept gets adopted, enhancement could be made down the road. 
