## Data Processing

The following scripts needs to be executed whenever you choose to build or refresh the catalog.

Order of execution
 - [export_account_usage_views.sql](../scripts/sflk_sql/export_account_usage_views.sql) 
 - [data_process.sql](../scripts/sflk_sql/data_process.sql) 
 - [data_discovery_tables.sql](../scripts/sflk_sql/data_discovery_tables.sql) 

## Script export_account_usage_views.sql
The script [scripts/sflk_sql/data_process.sql](../scripts/sflk_sql/export_account_usage_views.sql) performs the following:
 - Copies the various views from Snowflake Account_Usage schema to UTIL_DB.SFLK_CATALOG. We do this copy, as querying the views tend to be slow.

## Script data_process.sql
The script [scripts/sflk_sql/data_process.sql](../scripts/sflk_sql/data_process.sql) performs the following:

1. Copy the Snowflake.Account Usage views into a util_db.sflk_catalog schema.
2. Filter out records that currently offer fewer values. Like Show/Use/Describe commands etc.
3. Cleanse, parse and tokenize the query text from the Query_History table
4. Join back with the "Tables" & "Views" table to identify the tables, views, database, and schema used.

Do not expose these intermediary tables to the Snowflake public. As the data can contain sensitive informations.

## Script data_discovery_tables.sql
The script [scripts/sflk_sql/data_discovery_tables.sql](../scripts/sflk_sql/data_discovery_tables.sql) performs the following:

-  Build views/materialized table on the above table that offers various insights related to data catalog & data discovery.

You can choose the expose these table to public, as the sensitive informations are removed.